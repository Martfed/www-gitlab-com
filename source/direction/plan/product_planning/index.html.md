---
layout: markdown_page
title: "Product Direction - Product Planning"
description: Product Planning enables larger teams and organizations to manage product portfolios, requirements and other complex agile planning needs seamlessly in a single application. 
canonical_path: "/direction/plan/product_planning/"
---

- TOC
{:toc}



## Categories

### Epics
[Epics Direction](/direction/plan/epics/)
 
Epics allow you to manage your portfolio of projects more efficiently and with less effort by tracking groups of issues that share a theme, across projects and milestones. Nest multiple child epics under a parent epic to create deeper work structures that enable more flexible and granular planning.

#### What's Next for Epics

1. [Epic Swimlanes on Boards](https://gitlab.com/gitlab-org/gitlab/-/issues/7371)
1. [Program/Epic Level Boards](https://gitlab.com/groups/gitlab-org/-/epics/2864)

### Roadmaps
[Roadmaps Direction](/direction/plan/roadmaps/)

Visually plan and map projects in a roadmap that can be used for tracking and communication. GitLab provides timeline-based roadmap visualizations to enable users plan from small time scales (e.g. 2-week sprints for development teams) to larger time scales (e.g. quarterly or annual strategic initiatives for entire departments).

#### What's Next for Roadmaps

1. [Better Filtering on Roadmaps](https://gitlab.com/groups/gitlab-org/-/epics/2923)
1. [Surface Dependencies on Roadmap](https://gitlab.com/gitlab-org/gitlab/-/issues/33587)


### Requirements Management
[Requirements Management Direction](/direction/plan/requirements_management/)

Requirements Management enables documenting, tracing, and control of changes to agreed-upon requirements in a system. Our strategy is to make it simple and intuitive to create and trace your requirements throughout the entire Software DevOps lifecycle. 

#### What's next for Requirements Management

1. [Allow users to import requirements from other tools](https://gitlab.com/gitlab-org/gitlab/-/issues/233535)
1. [Results of User Research](https://gitlab.com/groups/gitlab-org/-/epics/3708) 

### Quality Management 
[Quality Management Direction](/direction/plan/quality_management)

Our goal for Quality management in GitLab is to allow for uses to track performance of test cases against their different environments over time, allowing for analysis of trends and identifiying critical failures prior to releasing to production.

#### What's next for Quality Management

The first step in building out Quality Management is a scaffolding framework for testing. In particular, we are calling these test cases, and test sessions. These will be first class native objects in GitLab, used to track the quality process of testing itself.

The MVC can be seen at [https://gitlab.com/groups/gitlab-org/-/epics/3852](https://gitlab.com/groups/gitlab-org/-/epics/3852). This work is currently ongoing, and we expect to have full support for test cases in GitLab 13.5, with additional support for test sessions in GitLab 13.6.

### Service Desk
[Service Desk](/direction/plan/service_desk)

Service desk allows your organization the opportunity to provide an email address to your customers. These customers can send issues, feature requests, comments, and suggestions via email, with no external tools needed. These emails become issues right inside GitLab, potentially even in the same project where you are developing your product or service, pulling your customers directly into your DevOps process.

#### What's next for Service Desk

1. [Allow private comments on all commentable resources](https://gitlab.com/groups/gitlab-org/-/epics/2697)
1. [Customize Service Desk to reflect our business, not GitLab specifically]

*The above plan can change at any moment and should not be taken as a hard commitment, though we do try to keep things generally stable. In general, we follow the same [prioritization guidelines](/handbook/product/product-processes/#how-we-prioritize-work) as the product team at large. Issues will tend to flow from having no milestone or epic, to being added to the backlog, to being added to this page and/or a specific milestone for delivery.*
